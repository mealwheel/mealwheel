package com.it390.mealwheel.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import com.it390.mealwheel.domain.FavoriteIngredient;

@Path("/favorite-ingredients")
public class FavoriteIngredientService {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addFavoriteIngredients(List<FavoriteIngredient> favoriteIngredients) {
		
		UserService userService = UserServiceFactory.getUserService();
		User googleUser = userService.getCurrentUser();
		
		if (googleUser != null) { // if user has logged in with their Google account
			if (!favoriteIngredients.isEmpty()) {
				for (FavoriteIngredient favoriteIngredient : favoriteIngredients) {
					if (favoriteIngredient.getEmail() != null && favoriteIngredient.getIngredient() != null) {
						ObjectifyService.ofy().save().entity(favoriteIngredient).now();
					}
				}
				return Response.status(Response.Status.OK).entity(favoriteIngredients).build();
			} else {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}
	
	@POST
	@Path("remove")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeFavoriteIngredients(FavoriteIngredient favoriteIngredient) {
		
		UserService userService = UserServiceFactory.getUserService();
		User googleUser = userService.getCurrentUser();
		
		if (googleUser != null) { // if user has logged in with their Google account
			if (favoriteIngredient != null) {
				ObjectifyService.ofy().delete().entity(favoriteIngredient).now();
				return Response.status(Response.Status.OK).build();
			} else {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFavoriteIngredients() {
		
		List<FavoriteIngredient> favoriteIngredients = new ArrayList<FavoriteIngredient>();
		
		UserService userService = UserServiceFactory.getUserService();
		User googleUser = userService.getCurrentUser();
		
		if (googleUser != null) { // if user has logged in with their Google account
			favoriteIngredients = ObjectifyService.ofy().load().type(FavoriteIngredient.class).filter("email", googleUser.getEmail()).order("ingredient").list();
			return Response.status(Response.Status.OK).entity(favoriteIngredients).build();
		} else {
			return Response.status(Response.Status.OK).build();
		}
		
	}
	
}