package com.it390.mealwheel.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import com.it390.mealwheel.domain.MealWheelUser;

@Path("/mealwheeluser")
public class MealWheelUserService {
	
	@GET
	@Path("/check-session")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkSession() {
		
		UserService userService = UserServiceFactory.getUserService();
		User googleUser = userService.getCurrentUser();
		
		boolean isLoggedIn;
		
		if (googleUser != null) { // if user has logged in with their Google account
			isLoggedIn = true;
		} else {
			isLoggedIn = false;
		}
		
		return Response.status(Response.Status.OK).entity(isLoggedIn).build();
	}
	
	@GET
	@Path("/get-user-info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInfo() {
		
		UserService userService = UserServiceFactory.getUserService();
		User googleUser = userService.getCurrentUser();
		
		if (googleUser != null) { // if user has logged in with their Google account
			MealWheelUser mealWheelUser = ObjectifyService.ofy().load().type(MealWheelUser.class).id(googleUser.getEmail()).now();
			
			if (mealWheelUser == null) {
				mealWheelUser = new MealWheelUser(googleUser.getEmail(), null, null);
			}
			
			return Response.status(Response.Status.OK).entity(mealWheelUser).build();
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		
	}

}