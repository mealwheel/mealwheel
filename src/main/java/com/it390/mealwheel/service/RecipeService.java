package com.it390.mealwheel.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/get-recipes")
public class RecipeService {
	
	
//	public static String excutePost(String targetURL, String urlParameters) {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRecipes(String ingredients) {
		ingredients = ingredients.replaceAll(" ", "%20");
		System.out.println(ingredients);
		String responseString = executePost("http://food2fork.com/api/search?key=c335eeac5f3046b1e28100c72e78dd0e&q=" + ingredients);
		return Response.status(200).entity(responseString).build();
	}
	public static String executePost(String targetURL) {
		HttpURLConnection connection = null;
		try {
			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
			dataOutputStream.close();

			// Get Response
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			StringBuilder response = new StringBuilder(); // or StringBuffer if
															// not Java 5+
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			bufferedReader.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
	
//	public static String excutePost(String targetURL, String urlParameters) {
//		HttpURLConnection connection = null;
//		try {
//			// Create connection
//			URL url = new URL(targetURL);
//			connection = (HttpURLConnection) url.openConnection();
//			connection.setRequestMethod("POST");
//			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//
//			connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
//			connection.setRequestProperty("Content-Language", "en-US");
//
//			connection.setUseCaches(false);
//			connection.setDoOutput(true);
//
//			// Send request
//			DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
//			dataOutputStream.writeBytes(urlParameters);
//			dataOutputStream.close();
//
//			// Get Response
//			InputStream inputStream = connection.getInputStream();
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//			StringBuilder response = new StringBuilder(); // or StringBuffer if
//															// not Java 5+
//			String line;
//			while ((line = bufferedReader.readLine()) != null) {
//				response.append(line);
//				response.append('\r');
//			}
//			bufferedReader.close();
//			return response.toString();
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		} finally {
//			if (connection != null) {
//				connection.disconnect();
//			}
//		}
//	}
	
}
