package com.it390.mealwheel.controller;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		if (user != null) {
			
			// if user is already logged in
			String redirectURL = userService.createLogoutURL("/");
		    resp.sendRedirect(redirectURL);
		
		} else {
			
			// if user is not logged in
			String redirectURL = userService.createLoginURL("/");
		    resp.sendRedirect(redirectURL);

		}
	}
	
}