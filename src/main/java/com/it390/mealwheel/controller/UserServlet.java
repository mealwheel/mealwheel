package com.it390.mealwheel.controller;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import com.it390.mealwheel.domain.MealWheelUser;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class UserServlet extends HttpServlet {

	  @Override
	  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

            UserService userService = UserServiceFactory.getUserService();
            User currentUser = userService.getCurrentUser();

            if (currentUser != null) {
            	String email = currentUser.getEmail();
                String first_name = request.getParameter("first_name");
                String last_name = request.getParameter("last_name");
                response.setContentType("text/plain");
                
                MealWheelUser mealWheelUser = new MealWheelUser(email, first_name,last_name);
                
                ObjectifyService.ofy().save().entity(mealWheelUser).now();
                
                response.sendRedirect("/#");
            }
	  }
}