package com.it390.mealwheel.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Ingredient {
	
	@Id Long Id;
	@Index String name;
	
	public Ingredient() {
		
	}
	
	public Ingredient(Long Id, String name) {
		this.Id = Id;
		this.name = name;
	}
	
	public void setId(Long Id) {
		this.Id = Id;
	}
	
	public Long getId() {
		return Id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}