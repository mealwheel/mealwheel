package com.it390.mealwheel.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class FavoriteIngredient {

	@Id Long Id;
	@Index String email;
	@Index String ingredient;
	
	public FavoriteIngredient() {
		
	}

	public FavoriteIngredient(Long Id, String email, String ingredient) {
		this.Id = Id;
		this.email = email;
		this.ingredient = ingredient;
	}
	
	public void setId(Long Id) {
		this.Id = Id;
	}
	
	public Long getId() {
		return Id;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}
	
	public String getIngredient() {
		return ingredient;
	}
	
}