package com.it390.mealwheel;

import com.googlecode.objectify.ObjectifyService;
import com.it390.mealwheel.domain.FavoriteIngredient;
import com.it390.mealwheel.domain.Ingredient;
import com.it390.mealwheel.domain.MealWheelUser;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;



/**
 * OfyHelper, a ServletContextListener, is setup in web.xml to run before a JSP is run.  This is
 * required to let JSP's access Ofy.
 **/
public class OfyHelper implements ServletContextListener {
  public void contextInitialized(ServletContextEvent event) {
    // This will be invoked as part of a warmup request, or the first user request if no warmup
    // request.
    ObjectifyService.register(MealWheelUser.class);
    ObjectifyService.register(FavoriteIngredient.class);
    ObjectifyService.register(Ingredient.class);
  }

  public void contextDestroyed(ServletContextEvent event) {
    // App Engine does not currently invoke this method.
  }
}