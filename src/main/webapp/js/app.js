// create the module and name it mealWheelApp
// also include ngRoute for all our routing needs
(function() {
    //console.log("in mealWheelapp");
    var mealWheelApp = angular.module('mealWheelApp', ['ngRoute']);

    // GLOBAL VARIABLES
    var isUserLoggedIn;
    var mealWheelUser = {};
    var appUrl;
    var allIngredients = null;

    if (location.hostname == "localhost") {
        appUrl = location.protocol + '//' + location.hostname + ':8080';
    } else {
        appUrl = location.protocol + '//' + location.hostname;
    }

    // configure our routes
    mealWheelApp.config(function($routeProvider) {
        $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

        // route for the profile page
        .when('/profile', {
            templateUrl: 'views/profile.jsp'
        });
    });


    //**********MAIN CONTROLLER***** create the controller and inject Angular's $scope
    mealWheelApp.controller('MainController', ['$scope', '$http', function($scope, $http) {
        //console.log("in mainController...");

        // scope variables
        $scope.discoveredRecipes;
        $scope.ingredientToAdd = [];
        $scope.ingredients = [];
        
        $scope.user = {};
        $scope.user.isLoggedIn;

        $scope.add = function(input) {
            var ingredient = {
                email: mealWheelUser.email,
                //ingredient: input.name
                ingredient: $("#tags").val()
            }

            if (isUserLoggedIn) {
                $scope.ingredientToAdd.push(ingredient);
                $scope.addToFavoriteIngredients(ingredient);
            } else {
                $scope.ingredients.push(ingredient);
            }

            input.name = '';

            $('#select-all').show();
        } //end $scope.add


        $scope.addToFavoriteIngredients = function(ingredientToAdd) {
            var req = {
                method: 'POST',
                url: appUrl + '/rest/favorite-ingredients',
                headers: {
                    'Content-Type': 'application/json',
                },
                data: $scope.ingredientToAdd
            }

            $http(req).then(function(response) {
                var req2 = {
                    method: 'GET',
                    url: appUrl + '/rest/favorite-ingredients',
                    headers: {
                        'Content-Type': 'application/json',
                    }
                };
                $http(req2).then(function(response) {
                    $scope.ingredientToAdd = [];
                    $scope.ingredients = [];
                    $scope.ingredients = response.data;
                }, function() {
                    console.log("ERROR");
                });
            }, function() {
                console.log("ERROR");
            });
        } //end $scope.addToFavoriteIngredients


        $scope.deleteFavoriteIngredient = function(ingredientToDelete) {
            if (isUserLoggedIn) {
                var req = {
                    method: 'POST',
                    url: appUrl + '/rest/favorite-ingredients/remove',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: ingredientToDelete
                }

                $http(req).then(function() {
                    var req = {
                        method: 'GET',
                        url: appUrl + '/rest/favorite-ingredients',
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    };
                    $http(req).then(function(response) {
                        $scope.ingredients = response.data;
                        if ($scope.ingredients.length == 0) {
                            $('#select-all').hide();
                        }
                    }, function() {
                        console.log("ERROR");
                    });
                }, function() {
                    console.log("ERROR");
                });
            } else {
                var arrayIndex = 0;
                $scope.ingredients.forEach(function(ingredientInArray) {
                    if (ingredientInArray == ingredientToDelete) {
                        $scope.ingredients.splice(arrayIndex, 1);
                    } else {
                        arrayIndex++;
                    }
                });
                if ($scope.ingredients.length == 0) {
                    $('#select-all').hide();
                }
            }
        } //end $scope.deleteFavoriteIngredient


        $scope.getFavorites = function() {
            var req = {
                method: 'GET',
                url: appUrl + '/rest/favorite-ingredients',
                headers: {
                    'Content-Type': 'application/json',
                }
            }

            $http(req).then(function(response) {
                $scope.ingredients = response.data;
                if ($scope.ingredients.length > 0) {
                    $('#select-all').show();
                }
            }, function() {
                console.log("ERROR");
            });
        } //end $scope.getFavorites


        $scope.discoverRecipes = function() {
            var ingredientsForDiscovery = "";
            var atLeastOneIngredientChecked = false;

            // discovers recipe with checked ingredients
            var checkedValue = null;
            var inputElements = document.getElementsByClassName('ingredientCheckbox');

            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    atLeastOneIngredientChecked = true;
                    ingredientsForDiscovery += inputElements[i].value + ",";
                }
            } //end FOR

            if (atLeastOneIngredientChecked == true) {
                var req = {
                    method: 'POST',
                    url: appUrl + '/rest/get-recipes',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: ingredientsForDiscovery
                }

                $http(req).then(function(response) {
                    $scope.discoveredRecipes = response.data.recipes;
                    if ($scope.discoveredRecipes.length === 0) {
                        alert("No recipes found. Please select different ingredients and try again.");
                    } else {
                        $scope.discoveredRecipes.forEach(function(discoveredRecipe) {
                            discoveredRecipe.title = discoveredRecipe.title.replace(/&amp;/gi, "&");
                        });
                        // Smooth scroll to recipe grid on click
                        $('html, body').animate({
                            scrollTop: $("#discover-recipe-grid").offset().top
                        }, 1500);
                    }
                }, function() {
                    console.log("ERROR");
                });
            } else {
                alert("Please select at least one ingredient to discover a recipe.");
            }
        } //end $scope.discoverRecipes

        $scope.selectAllIngredients = function() {
            $('.ingredientCheckbox').prop('checked', true);
            $('#select-all').hide();
            $('#unselect-all').show();
        }

        $scope.unselectAllIngredients = function() {
            $('.ingredientCheckbox').prop('checked', false);
            $('#unselect-all').hide();
            $('#select-all').show();
        }
        
        $scope.getUserInfo = function(response) {
            var req = {
                method: 'GET',
                url: appUrl + '/rest/mealwheeluser/get-user-info',
                headers: {
                    'Content-Type': 'application/json',
                }
            };

            $http(req).then(function(response) {
                $scope.user.info = response.data;
                mealWheelUser = response.data;
            }, function() {
                console.log("ERROR");
            });
        } //end $scope.getUserInfo

        $scope.checkIfLoggedIn = function(response) {
            var req = {
                method: 'GET',
                url: appUrl + '/rest/mealwheeluser/check-session',
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            $http(req).then(function(response) {
                if (response.data == "true") {
                    $scope.getUserInfo();
                    $scope.user.isLoggedIn = true;
                    isUserLoggedIn = true;
                    $scope.getFavorites();
                } else {
                    $scope.user.isLoggedIn = false;
                    isUserLoggedIn = false;
                }
            }, function() {
                console.log("ERROR");
            });
        } //end $scope.checkIfLoggedIn
        
//        $scope.addIngredientsToObjectify = function(response) {
//            var req = {
//                method: 'GET',
//                url: appUrl + '/rest/ingredient/add-to-objectify',
//                headers: {
//                    'Content-Type': 'application/json',
//                }
//            };
//            $http(req).then(function(response) {
//                console.log(response);
//            }, function() {
//                console.log("ERROR");
//            });
//        } //end $scope.addIngredientsToObjectify
        
        $scope.getAllIngredients = function(response) {
            var req = {
                method: 'GET',
                url: appUrl + '/rest/ingredients',
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            $http(req).then(function(response) {
                allIngredients = response.data;
                $("#tags").autocomplete({
                    source: allIngredients,
                    minLength: 1
                });
            }, function() {
                console.log("ERROR");
            });
        } //end $scope.checkIfLoggedIn

    	/* ****************EXECUTE**************** */
        if (allIngredients == null) {
        	allIngredients = [];
        	$scope.getAllIngredients();
        }
        
        $scope.checkIfLoggedIn();

        $('#select-all').hide();
        $('#unselect-all').hide();
    }]);
})();