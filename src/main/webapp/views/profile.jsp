<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>

<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>

<%@ page import="com.googlecode.objectify.ObjectifyService" %>

<%@ page import="com.it390.mealwheel.domain.MealWheelUser"%>

<%@ page import="java.util.List"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="row" style="min-height:500px; margin-top:100px;">
	<div class="col-md-2 col-md-offset-5">
	
		<%
		UserService userService = UserServiceFactory.getUserService();
	
		User googleUser = userService.getCurrentUser();
		
		MealWheelUser mealWheelUser = ObjectifyService.ofy().load().type(MealWheelUser.class).id(googleUser.getEmail()).now();
	
		if (googleUser != null) {
	
			pageContext.setAttribute("googleUser", googleUser);
			
			if (mealWheelUser != null) {
				pageContext.setAttribute("mealWheelUser", mealWheelUser);
				pageContext.setAttribute("first_name", mealWheelUser.getFirstName());
				pageContext.setAttribute("last_name", mealWheelUser.getLastName());
			}
	
		} else {

			String site = "/login";
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);

		}
		%>
		
		<!-- PROFILE FORM -->
		<div>
			<form action="/user-servlet" method="get">
		        <div class="form-group">
		            <label for="first_name">First name</label>
		            <input type="text" class="form-control" name="first_name" value="${fn:escapeXml(first_name)}" required />
		        </div>
		        <div class="form-group">
		            <label for="last_name">Last name</label>
		            <input type="text" class="form-control" name="last_name" value="${fn:escapeXml(last_name)}" required />
		        </div>
	            <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
		    </form>
		</div>
		<!-- END PROFILE FORM -->

    </div><!-- END COLUMN -->
</div><!-- END ROW -->
